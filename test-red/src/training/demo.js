import { createStore } from 'redux';
import { status, sort } from './actions/index';
import myReducer from './reducers/index';

const store = createStore(myReducer);
console.log('defautl:', store.getState());

// var action = { type: 'toogle_status'};
store.dispatch(status());

console.log('toogle_status:', store.getState());

// var sortAction = {
//     type: 'sort',
//     sort: {
//         by: 'baovic',
//         value: -1
//     }
// }
var sortAction = {
        by: 'baovic',
        value: -1
    }
store.dispatch(sort(sortAction));
console.log('sort:', store.getState())
// 

// component co cac thao tac nhu submitform...goi la cac action
// store se goi function dispatch de no gui action vao reducer
// action(nhu mua hang..) se truyen toi reducer, reducer xuly, va dua vao trong store, store(quan ly cac state) se tao ra cac state moi
// va tra ve Component.
